<?php
$wgAutoloadClasses['Communecter'] = $IP . '/extensions/CoInWiki/communecter_body.php';
$wgHooks['ParserFirstCallInit'][] = 'Communecter::onParserInit';

$wgResourceModules['communecter'] = array(
	'scripts' => 'resources/communecter.js',
	'localBasePath' => __DIR__,
	'remoteExtPath' => 'CoInWiki',
	'styles' => 'resources/communecter.css'
);