<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @file
 */

namespace MediaWiki\Extension\CoInWiki;

class Hooks implements 

\MediaWiki\Hook\BeforePageDisplayHook,
\Mediawiki\Hook\UserLoginCompleteHook
 {

	/**
	 * @param \OutputPage $out
	 * @param \Skin $skin
	 */
	public function onBeforePageDisplay( $out, $skin ): void {
		$config = $out->getConfig();
		if ( $config->get( 'CoInWikiConnect' ) ) {
			$out->addHTML( \Html::element( 'p', [], 'CoInWiki was here' ) );
			$out->addModules( 'ext.coInWiki' );

		}
	}

	public function onUserLoginComplete( $user, &$inject_html, $direct ) {
		// $inject_html = \Html::element( 'div', [], var_dump($user) );
		return;
	}

}
